# 门神

#### 介绍
门神(menshen)是一个基于javaagent技术实现的进行类库特定权限校验和拦截的组件。以前我们对于第三方类库总是无条件的信任，但近期的log4j事件发生时很多人都在想一个日志库为什么要有网络功能，这说明对第三方类库的权限控制对于安全同样重要，不管是防范第三方类库存在的漏洞还是来自其他方面的恶意攻击……

门神限制log4j网络权限示例：
![输入图片说明](%E5%BE%AE%E4%BF%A1%E6%88%AA%E5%9B%BE_20220108144915.png)

#### 软件架构
maven、javaagent、javassist

javaagent技术提供了在类加载时修改class的机制，门神基于javaagent在类加载时对关键的javaapi方法添加监控，运行时检查堆栈调用来控制特定类库的调用权限。比如：执行本地命令、加载本地类库、文件读写、数据库访问以及网络调用权限等。


#### 使用说明

要使用门神需要在java应用启动时添加javaagent VM参数，如：`-javaagent:C:\xxx\menshen\menshen-agent\target\menshen-agent-1.0-SNAPSHOT.jar`

#### 项目结构说明

此项目采用maven构建，其中menshen-agent是门神的核心代码，menshen-simpletest是用于测试门神相关功能的模块。

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

