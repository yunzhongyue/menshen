package site.ruyi.menshen.simpletest;

public class NativePermissionTest {

    public static void main(String[] args) {
        testInterceptSystemLoad();
        testInterceptSystemLoadLibrary();
        testInterceptRuntimeLoad();
        testInterceptRuntimeLoadLibrary();
    }

    /**
     * 测试拦截System.load加载本地库
     */
    private static void testInterceptSystemLoad(){
        try{
            System.load("aaa.so");
            throw new Error("menshen intercept error!");
        }catch (Exception e){
            if(e instanceof RuntimeException && e.getMessage().contains("deny by menshen!")){
                System.out.println("System.load intercept OK!");
            }else{
                throw new RuntimeException(e);
            }
        }
    }

    /**
     * 测试拦截System.loadLibrary加载本地库
     */
    private static void testInterceptSystemLoadLibrary(){
        try{
            System.loadLibrary("aaa");
            throw new Error("menshen intercept error!");
        }catch (Exception e){
            if(e instanceof RuntimeException && e.getMessage().contains("deny by menshen!")){
                System.out.println("System.loadLibrary intercept OK!");
            }else{
                throw new RuntimeException(e);
            }
        }
    }

    /**
     * 测试拦截Runtime.load加载本地库
     */
    private static void testInterceptRuntimeLoad(){
        try{
            Runtime.getRuntime().load("aaa.so");
            throw new Error("menshen intercept error!");
        }catch (Exception e){
            if(e instanceof RuntimeException && e.getMessage().contains("deny by menshen!")){
                System.out.println("Runtime.load intercept OK!");
            }else{
                throw new RuntimeException(e);
            }
        }
    }

    /**
     * 测试拦截Runtime.loadLibrary加载本地库
     */
    private static void testInterceptRuntimeLoadLibrary(){
        try{
            Runtime.getRuntime().loadLibrary("aaa");
            throw new Error("menshen intercept error!");
        }catch (Exception e){
            if(e instanceof RuntimeException && e.getMessage().contains("deny by menshen!")){
                System.out.println("Runtime.loadLibrary intercept OK!");
            }else{
                throw new RuntimeException(e);
            }
        }
    }
}
