package site.ruyi.menshen.simpletest;

public class CmdPermissionTest {

    public static void main(String[] args) {
        testInterceptRuntimeExec();
        testInterceptProcessBuilderStart();
    }

    /**
     * 测试拦截Runtime.exec执行系统命令
     */
    private static void testInterceptRuntimeExec(){
        try{
            Runtime.getRuntime().exec("ping www.baidu.com");
            throw new Error("menshen intercept error!");
        }catch (Exception e){
            if(e instanceof RuntimeException && e.getMessage().contains("deny by menshen!")){
                System.out.println("Runtime.exec intercept OK!");
            }else{
                throw new RuntimeException(e);
            }
        }
    }

    /**
     * 测试拦截ProcessBuilder.start执行系统命令
     */
    private static void testInterceptProcessBuilderStart(){
        try{
            ProcessBuilder pb=new ProcessBuilder(new String[]{"ping","www.baidu.com"});
            pb.start();
            throw new Error("menshen intercept error!");
        }catch (Exception e){
            if(e instanceof RuntimeException && e.getMessage().contains("deny by menshen!")){
                System.out.println("ProcessBuilder.start intercept OK!");
            }else{
                throw new RuntimeException(e);
            }
        }
    }
}
