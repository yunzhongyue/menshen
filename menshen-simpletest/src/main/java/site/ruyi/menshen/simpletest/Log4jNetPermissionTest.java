package site.ruyi.menshen.simpletest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Log4jNetPermissionTest {

    public static void main(String[] args) {
        /**
         * log4j网络权限拦截测试
         */
        Logger logger = LoggerFactory.getLogger(Log4jNetPermissionTest.class);
        logger.error("${jndi:ldap://127.0.0.1:1001/test}");
    }
}
